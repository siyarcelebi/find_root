"""
PYTHON SQUARE ROOT CALCULATOR
"""
import sys


def main():

	print('\nCALCULATE THE SQUARE ROOT or CUBE ROOT OF ANY NUMBER \n')

	while True:

		print """
		1. Find a square root of any number
		2. Find a cube root of any number 
		3. Exit \n 
		"""

		choice = raw_input('Please choose one of the options above.\n')

		if choice == '1':

			number = float(raw_input('Please enter a number:\n'))

			squere_root = number ** 0.5 # Since the squere root of any number is equal to 1/2 squeare of that number.

			print("\nSquare Root of %0.2f is %0.2f \n" %(number, squere_root))

		if choice == '2':

			number = float(raw_input('Please enter a number:\n'))

			cube_root = number ** 0.3333 # Since the cube root of any number is equal to 1/3 squeare of that number.

			print("\nCube Root of %0.2f is %0.2f \n" %(number, cube_root))


		if choice == '3':
			sys.exit()

main()
